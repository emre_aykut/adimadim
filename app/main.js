import Vue from 'nativescript-vue'
import store from './store'
import router from './router'
import VueDevtools from 'nativescript-vue-devtools'
import { TNSFontIcon, fonticon } from 'nativescript-fonticon';
import * as platform from 'tns-core-modules/platform'
import DateTimePicker from "nativescript-datetimepicker/vue";
import moment from 'moment'
import RadListView from 'nativescript-ui-listview/vue';
import { Gif } from "nativescript-gif";
import * as application from "tns-core-modules/application";



// var firebase = require("nativescript-plugin-firebase");

// firebase.init({
//     onMessageReceivedCallback: function(message) {
//         console.log("Title: " + message.title);
//         console.log("Body: " + message.body);
//         // if your server passed a custom property called 'foo', then do this:
//         console.log("Value of 'foo': " + message.data.foo);
//       },
//       showNotificationsWhenInForeground: true,
// }).then(
//     function () {
//       console.log("firebase.init done");
//     },
//     function (error) {
//       console.log("firebase.init error: " + error);
//     }
// );


Vue.use(DateTimePicker);
Vue.use(RadListView);

let trLocale = require('moment/locale/tr');
moment.locale('tr', trLocale)

if (platform.isIOS) {
    Vue.registerElement("PreviousNextView", () => require("nativescript-iqkeyboardmanager").PreviousNextView)
    IQKeyboardManager.sharedManager().toolbarDoneBarButtonItemText = "Tamam"
    IQKeyboardManager.sharedManager().enable = true
    GMSServices.provideAPIKey('AIzaSyASgK-EyVswAr3dln86X7_LtpgWYN9Pf1c')
}

Vue.registerElement("Gif", () => Gif);
Vue.registerElement('NumericKeyboard', () => require('nativescript-numeric-keyboard').NumericKeyboardView);
Vue.registerElement('AnimatedCircle', () => require('nativescript-animated-circle').AnimatedCircle),
Vue.registerElement('SVGImage', () => require('nativescript-svg').SVGImage);
Vue.registerElement('MapView', () => require('nativescript-google-maps-sdk').MapView),
Vue.registerElement("DropDown", () => require("nativescript-drop-down/drop-down").DropDown)
Vue.registerElement('Carousel', () => require('nativescript-carousel').Carousel),
Vue.registerElement('CarouselItem', () => require('nativescript-carousel').CarouselItem)
<<<<<<< HEAD
// Vue.registerElement("PreviousNextView", () => require("nativescript-iqkeyboardmanager").PreviousNextView)
=======
>>>>>>> feabd91075e20d375c6829470bb454362f9bf32c
Vue.registerElement('CheckBox', () => require('@nstudio/nativescript-checkbox').CheckBox, {
    model: {
        prop: 'checked',
        event: 'checkedChange'
    }
});

<<<<<<< HEAD
// IQKeyboardManager.sharedManager().toolbarDoneBarButtonItemText = "Tamam"
// IQKeyboardManager.sharedManager().enable = true
=======

>>>>>>> feabd91075e20d375c6829470bb454362f9bf32c

Vue.filter('fonticon', fonticon);
TNSFontIcon.debug = false;
TNSFontIcon.paths = {

    'la': './assets/line-awesome.min.css',
};
TNSFontIcon.loadCss();

if (TNS_ENV !== 'production') {
    Vue.use(VueDevtools)
}

// Vue.config.silent = (TNS_ENV === 'production')
Vue.prototype.$store = store

Vue.prototype.$router = router
Vue.prototype.$goto = function (to, options) {
    var options = options || {
        clearHistory: false,
        backstackVisible: true,
        transition: {
            name: "fade",
            duration: 1,
            curve: "easeIn"
        }
    }
    this.$navigateTo(this.$router[to], options)
}

const frameModule = require("tns-core-modules/ui/frame");
function onNavBtnTap() {
    frameModule.topmost().goBack();
}
exports.onNavBtnTap = onNavBtnTap;

let defaultUrl = ''
if (store.getters.loggedIn) {
    defaultUrl = 'raceList'
} else {
    defaultUrl = 'home'
}

new Vue({
    store,
    render: h => h('frame', [h(router[defaultUrl])])
}).$start()

