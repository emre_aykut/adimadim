import Vue from 'nativescript-vue';
import Vuex from 'vuex';
import axios from 'axios'

import appAuth from "./app/auth.module";
import appProfile from "./app/profile.module";
import appRace from "./app/race.module";
import appTeam from "./app/team.module";
import appResult from "./app/result.module";
import appNotification from "./app/notification.module";
import appAdmin from "./app/admin.module";

Vue.use(Vuex);

axios.defaults.baseURL = 'http://adimadim.strajedi.com.tr/api'
// axios.defaults.baseURL = 'http://192.168.0.45:1007/api'

export default new Vuex.Store({
    
    modules: {
        appAuth,
        appProfile,
        appRace,
        appTeam,
        appResult,
        appNotification,
        appAdmin,
    },

    state: {
        
    },

    mutations: {
        
    },

    actions: {

    },

    getters: {
        
    }

});