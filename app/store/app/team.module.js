import axios from 'axios'
import localStorage from "nativescript-localstorage";

const state = {
    token: localStorage.getItem('access_token') || null,
    userId: localStorage.getItem('user_id') || null,
}

const getters = {
    
}

const mutations = {
    setToken(state, token) {
        state.token = token
    },
    setUserId(state, userId) {
        state.userId = userId
    },
}

const actions = {
    getToken(context){
        context.commit('setToken', localStorage.getItem('access_token'))
    },

    getUserId(context){
        context.commit('setUserId', localStorage.getItem('user_id'))
    },

    getTeams(context, data)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.get('/team' , {
                params: {
                    race_id: data.raceId,
                    search_text: data.searchText,
                }
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    getTeam(context, data)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.get('/team/' + context.state.userId , {
                params: {
                    race_id: data.raceId,
                }
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    getTeamCaptain(context, data)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.get('/team_user/is_captain', {
                params: {
                    team_id: data.teamId,
                    user_id: context.state.userId,
                }
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    registerForTeam(context, data)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.post('/team_user/register_request' , {
                team_id: data.teamId,
                race_id: data.raceId,
                user_id: context.state.userId
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    teamCreate(context, data)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.post('/team/create' , {
                name: data.teamName,
                race_id: data.raceId,
                user_id: context.state.userId
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    getUsersInTeam(context, data)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.get('/team/users', {
                params: {
                    search_text: data.searchText,
                    user_id: context.state.userId,
                    race_id: data.raceId,
                }
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
    
    inviteToTeam(context, data)
    {
        return new Promise((resolve, reject) => {
            axios.post('/team_user/create' , {
                team_users: data.teamUsers,
                team_id: data.teamId,
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    getTeamInfo(context, teamId)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.get('/team/info/' + teamId)
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    getUsersInTeamUser(context, teamId)
    {
        return new Promise((resolve, reject) => {
            axios.get('/team_user/users/' + teamId)
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    teamUserApprove(context, data)
    {
        return new Promise((resolve, reject) => {
            axios.post('/team_user/user_approve' , {
                team_id: data.teamId,
                user_id: data.userId,
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
}

export default {
    state,
    getters,
    mutations,
    actions,
};