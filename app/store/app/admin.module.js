import axios from 'axios'
import localStorage from "nativescript-localstorage";

const state = {
    token: localStorage.getItem('access_token') || null,
    userId: localStorage.getItem('user_id') || null,
    adminGroupId: localStorage.getItem('admin_group_id') || null,
}

const getters = {
    
}

const mutations = {
    setToken(state, token) {
        state.token = token
    },
    
    setUserId(state, userId) {
        state.userId = userId
    },

    setAdminGroupId(state, adminGroupId) {
        state.adminGroupId = adminGroupId
    },
}

const actions = {
    getToken(context){
        context.commit('setToken', localStorage.getItem('access_token'))
    },

    getUserId(context){
        context.commit('setUserId', localStorage.getItem('user_id'))
    },

    getAdminGroupId(context){
        context.commit('setAdminGroupId', localStorage.getItem('admin_group_id'))
    },

    getAdminRaces(context)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.get('/admin/race', {
                params:{
                    user_id: context.state.userId,
                    admin_group_id: context.state.adminGroupId,
                }
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    getAdminRacesFilter(context, data)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.get('/admin/race/filter', {
                params:{
                    city_id: data.cityId, 
                    month_id: data.monthId,
                    user_id: context.state.userId,
                    admin_group_id: context.state.adminGroupId,
                }
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
    
    adminRaceAdd(context, data)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.post('/admin/race/add', {
                title: data.title,
                trail_id: data.trail,
                start_date: data.startDate,
                race_type: data.raceType,
                start_hour: data.startHour,
                end_hour: data.endHour,
                min_time: data.minTime,
                max_time: data.maxTime,
                description: data.description,
                user_id: context.state.userId,
                admin_group_id: context.state.adminGroupId
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    adminRaceUpdate(context, data)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.put('/admin/race/update/' + data.raceId, {
                title: data.title,
                trail_id: data.trail,
                start_date: data.startDate,
                race_type: data.raceType,
                start_hour: data.startHour,
                end_hour: data.endHour,
                min_time: data.minTime,
                max_time: data.maxTime,
                description: data.description,
                user_id: context.state.userId,
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    getRegisteredUsers(context, data)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.get('/admin/race_user/list' , {
                params: {
                    race_id: data.raceId,
                    filter_type: data.filterType,
                    filter_val: data.filterVal,
                }
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    getRegisteredTeams(context, data)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.get('/admin/team/list' , {
                params: {
                    race_id: data.raceId,
                }
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    adminRaceDelete(context, raceId)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.delete('/admin/race/remove/' + raceId)
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
    
    adminRaceStart(context, raceId)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.post('/admin/race/start/' + raceId,{
                admin_group_id: context.state.adminGroupId,
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    adminRaceFinish(context, raceId)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.post('/admin/race/finish/' + raceId,{
                admin_group_id: context.state.adminGroupId,
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    adminTrailList(context)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        return new Promise((resolve, reject) => {
            axios.get('/admin/trail', {
                params:{
                    admin_group_id: context.state.adminGroupId,
                }
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    getTrailDetail(context, data)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.get('/admin/trail/' + data.id, {
                params:{
                    admin_group_id: context.state.adminGroupId,
                }
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    adminTrailAdd(context, data)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.post('/admin/trail/add', {
                name: data.name,
                racecourse: data.racecourse,
                city_id: data.cityId,
                open_address: data.openAddress,
                start_latitude: data.startLatitude,
                start_longitude: data.startLongitude,
                end_latitude: data.endLatitude,
                end_longitude: data.endLongitude,
                user_id: context.state.userId,
                admin_group_id: context.state.adminGroupId
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    adminTrailUpdate(context, data)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.put('/admin/trail/update/' + data.trailId, {
                name: data.name,
                racecourse: data.racecourse,
                city_id: data.cityId,
                open_address: data.openAddress,
                start_latitude: data.startLatitude,
                start_longitude: data.startLongitude,
                end_latitude: data.endLatitude,
                end_longitude: data.endLongitude,
                user_id: context.state.userId,
                admin_group_id: context.state.adminGroupId
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    adminTrailDelete(context, trailId)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.delete('/admin/trail/remove/' + trailId)
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    adminUserTimeUpdate(context, data)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token

        return new Promise((resolve, reject) => {
            axios.put('/admin/race_user/time_update/' + data.raceUserId, {
                finishing_time: data.finishingTime,
                race_id: data.raceId,
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
}

export default {
    state,
    getters,
    mutations,
    actions,
};