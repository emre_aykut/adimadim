import axios from 'axios'
import localStorage from "nativescript-localstorage";

const state = {
    token: localStorage.getItem('access_token') || null,
    userId: localStorage.getItem('user_id') || null,
}

const getters = {
    
}

const mutations = {
    setToken(state, token) {
        state.token = token
    },
    
    setUserId(state, userId) {
        state.userId = userId
    },
}

const actions = {
    getToken(context){
        context.commit('setToken', localStorage.getItem('access_token'))
    },

    getUserId(context){
        context.commit('setUserId', localStorage.getItem('user_id'))
    },

    getUser(context)
    {
        axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.get('/profile')
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    profileUpdate(context, data)
    {
        axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.put('/profile/' + data.id, {
                tc_no: data.tc_no,
                email: data.email,
                name: data.name,
                surname: data.surname,
                gender: data.gender,
                birth_date: data.birth_date,
                country_id: data.country_id,
                city_id: data.city_id,
                district_id: data.district_id,
                phone: data.phone,
                company_text: data.company_text,
                job_text: data.job_text,
                height: data.height,
                weight: data.weight,
                longest_running_distance: data.longest_running_distance,
                website: data.website,
                facebook_username: data.facebook_username,
                instagram_username: data.instagram_username,
                linkedin_username: data.linkedin_username,
                about: data.about,
                i_want_caylaklar_info: data.i_want_caylaklar_info,
                i_want_adimadimkosuaktivitesi_info: data.i_want_adimadimkosuaktivitesi_info,
                i_want_kurumsalkosutakimi_info: data.i_want_kurumsalkosutakimi_info,
                i_want_gonulluolmak_info: data.i_want_gonulluolmak_info,
                favorite_trails: data.favorite_trails,
                sports: data.sports,
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    districtAndCityConvert(context, data)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.get('/profile/district_and_city_convert', {
                params: {
                    city_id: data.cityId,
                    district_id: data.districtId,
                }
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    countryList(context)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token

        return new Promise((resolve, reject) => {
            axios.get('/profile/countries')
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    cityList(context, countryId)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token

        return new Promise((resolve, reject) => {
            axios.get('/profile/cities/' + countryId)
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    districtList(context, cityId)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token

        return new Promise((resolve, reject) => {
            axios.get('/profile/districts/' + cityId)
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    getStatistics(context)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token

        return new Promise((resolve, reject) => {
            axios.get('/profile/statistics', {
                params: {
                    user_id: context.state.userId
                }
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    profileInfoControl(context)
    {
        axios.defaults.headers.common['Authorization'] = context.state.token

        return new Promise((resolve, reject) => {
            axios.get('/profile/info_control', {
                params: {
                    user_id: context.state.userId
                }
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    isAdmin(context)
    {
        axios.defaults.headers.common['Authorization'] = context.state.token

        return new Promise((resolve, reject) => {
            axios.get('/profile/is_admin', {
                params: {
                    user_id: context.state.userId
                }
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    }
}

export default {
    state,
    getters,
    mutations,
    actions,
};