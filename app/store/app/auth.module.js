import axios from 'axios'
import localStorage from "nativescript-localstorage";

const state = {
    token: localStorage.getItem('access_token') || null,
    userId: localStorage.getItem('user_id') || null,
    adminGroupId: localStorage.getItem('admin_group_id') || null,
}

const getters = {
    loggedIn(state) {
        return state.token !== null
    },
}

const mutations = {
    retrieveToken(state, data) {
        state.token = data.token
        state.userId = data.userId
    },
    retrieveGroupId(state, data)
    {
        state.adminGroupId = data.adminGroupId
    },
    destroyToken(state) {
        state.token = null
        state.userId = null
        state.adminGroupId = null
    },
}

const actions = {

    register(context, data) {
        return new Promise((resolve, reject) => {
            axios.post('/auth/register', {
                name: data.name,
                surname: data.surname,
                email: data.email,
                password: data.password,
                is_contract: data.is_contract,
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    destroyToken(context) {
        axios.defaults.headers.common['Authorization'] = context.state.token

        if (context.getters.loggedIn) {
            return new Promise((resolve, reject) => {
                axios.post('/auth/logout')
                .then(response => {
                    localStorage.removeItem('access_token')
                    localStorage.removeItem('user_id')
                    localStorage.removeItem('admin_group_id')
                    context.commit('destroyToken')
                    resolve(response)
                })
                .catch(error => {
                    localStorage.removeItem('access_token')
                    localStorage.removeItem('user_id')
                    localStorage.removeItem('admin_group_id')
                    context.commit('destroyToken')
                    reject(error)
                })
            })
        }
    },

    retrieveToken(context, user) {
        return new Promise((resolve, reject) => {
            axios.post('/auth/login', {
                email: user.email,
                password: user.password,
            })
            .then(response => {
                const token = response.data.access_token
                const userId = response.data.id
                const adminGroupId = response.data.admin_group_id
                localStorage.setItem('access_token', token)
                localStorage.setItem('user_id', userId)
                localStorage.setItem('admin_group_id', adminGroupId)
                context.commit('retrieveToken', {token: token, userId: userId})
                if(response.data.admin_group_id)
                {
                    context.commit('retrieveGroupId', {adminGroupId: adminGroupId})
                }
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

}

export default {
    state,
    getters,
    mutations,
    actions,
};