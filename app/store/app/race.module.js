import axios from 'axios'
import localStorage from "nativescript-localstorage";
import moment from 'moment';

const state = {
    token: localStorage.getItem('access_token') || null,
    userId: localStorage.getItem('user_id') || null,
    totalSeconds: 0,
    secondsLabel: '',
    minutesLabel: '',
    notificationCount: '',
}

const getters = {
    notificationCount(state)
    {
        return state.notificationCount
    }
}

const mutations = {
    setToken(state, token)
    {
        state.token = token
    },

    setUserId(state, userId)
    {
        state.userId = userId
    },

    setNotificationCount(state, count)
    {
        state.notificationCount = count 
    },
}

const actions = {
    getToken(context)
    {
        context.commit('setToken', localStorage.getItem('access_token'))
    },

    getUserId(context)
    {
        context.commit('setUserId', localStorage.getItem('user_id'))
    },

    getNotificationCountByTimer(context)
    {
        const timerModule = require("tns-core-modules/timer");

        if(typeof timerModule != "undefined" || timerModule != null)
        {
            timerModule.clearInterval(intervalID);
        }

        new Promise((resolve, reject) => {
            axios.get('/notification/unviewed_list', {
                params: {
                    receiver_user_id: context.state.userId,
                }
            })
            .then(response => {
                let count = response.data
                if(response.data == 0)
                {
                    count = ''
                }
                context.commit('setNotificationCount', count)
            })
        })

        var intervalID = timerModule.setInterval(() => {

            // axios.defaults.headers.common['Authorization'] = context.state.token

            new Promise((resolve, reject) => {
                axios.get('/notification/unviewed_list', {
                    params: {
                        receiver_user_id: context.state.userId,
                    }
                })
                .then(response => {
                    let count = response.data
                    if(response.data == 0)
                    {
                        count = ''
                    }
                    context.commit('setNotificationCount', count)
                })
            })

        }, 10000);
    },

    getNotificationCount(context)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.get('/notification/unviewed_list', {
                params: {
                    receiver_user_id: context.state.userId,
                }
            })
            .then(response => {
                let count = response.data
                if(response.data == 0)
                {
                    count = ''
                }
                context.commit('setNotificationCount', count)
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    getRaces(context)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.get('/race', {
                params:{
                    user_id: context.state.userId,
                }
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    getRacesFilter(context, data)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.get('/race/filter', {
                params:{
                    city_id: data.cityId, 
                    month_id: data.monthId,
                    user_id: context.state.userId,
                }
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
    
    getRaceDetail(context, data)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.get('/race/' + data.id, {
                params:{
                    user_id: context.state.userId,
                }
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    raceRegister(context, data)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.post('/race_user/register' , {
                race_id: data.raceId,
                user_id: context.state.userId,
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
    
    raceStart(context, data)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.post('/race_user/start' , {
                race_id: data.raceId,
                user_id: context.state.userId,
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    raceEnd(context, data)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.post('/race_user/end' , {
                race_id: data.raceId,
                user_id: context.state.userId,
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    isThereRaceRegister(context, data)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token

        return new Promise((resolve, reject) => {
            axios.get('/race_user/user' , {
                params: {
                    race_id: data.raceId,
                    user_id: context.state.userId,
                }
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    registeredUsers(context, data)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token

        return new Promise((resolve, reject) => {
            axios.get('/race_user/users/' +  data.raceId, {
                params: {
                    search_text: data.searchText,
                }
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    raceAddCalendar(context, data)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.post('/race_user/add_calendar' , {
                race_id: data.raceId,
                user_id: context.state.userId,
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    getUsersInRace(context, data)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.get('/race/users', {
                params: {
                    search_text: data.searchText,
                    user_id: context.state.userId,
                    race_id: data.raceId,
                }
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    inviteToRace(context, data)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token

        return new Promise((resolve, reject) => {
            axios.post('/race_user/create' , {
                race_users: data.raceUsers,
                race_id: data.raceId,
                user_id: context.state.userId
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
}

export default {
    state,
    getters,
    mutations,
    actions,
};