import axios from 'axios'
import localStorage from "nativescript-localstorage";

const state = {
    token: localStorage.getItem('access_token') || null,
    userId: localStorage.getItem('user_id') || null,
}

const getters = {
    
}

const mutations = {
    setToken(state, token) {
        state.token = token
    },
    setUserId(state, userId) {
        state.userId = userId
    },
}

const actions = {
    getToken(context){
        context.commit('setToken', localStorage.getItem('access_token'))
    },

    getUserId(context){
        context.commit('setUserId', localStorage.getItem('user_id'))
    },

    getRegisteredResults(context)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.get('/race_user' , {
                params: {
                    user_id: context.state.userId,
                }
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    getResults(context)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.get('/race/results' , {
                params: {
                    user_id: context.state.userId,
                }
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    getUserStatistic(context, data)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.get('/race_user/statistic' , {
                params: {
                    race_id: data.raceId,
                    filter_type: data.filterType,
                    filter_val: data.filterVal,
                }
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    getTeamStatistic(context, data)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.get('/team/statistic' , {
                params: {
                    race_id: data.raceId,
                }
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    bestRunning(context)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.get('/race_user/best_running' , {
                params: {
                    user_id: context.state.userId,
                }
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
}

export default {
    state,
    getters,
    mutations,
    actions,
};