import axios from 'axios'
import localStorage from "nativescript-localstorage";

const state = {
    token: localStorage.getItem('access_token') || null,
    userId: localStorage.getItem('user_id') || null,
}

const getters = {
    
}

const mutations = {
    setToken(state, token) {
        state.token = token
    },
    setUserId(state, userId) {
        state.userId = userId
    },
}

const actions = {
    getToken(context){
        context.commit('setToken', localStorage.getItem('access_token'))
    },

    getUserId(context){
        context.commit('setUserId', localStorage.getItem('user_id'))
    },

    getNotifications(context)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.get('/notification', {
                params: {
                    receiver_user_id: context.state.userId,
                }
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    getNotificationsByViewed(context)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.get('/notification', {
                params: {
                    receiver_user_id: context.state.userId,
                    viewed: '0',
                }
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    allViewed(context)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.post('/notification/all_viewed', {
                receiver_user_id: context.state.userId,
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    readed(context, notificationId)
    {
        // axios.defaults.headers.common['Authorization'] = context.state.token
        
        return new Promise((resolve, reject) => {
            axios.post('/notification/readed', {
                notification_id: notificationId
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    acceptTeamInvitation(context, data)
    {
        return new Promise((resolve, reject) => {
            axios.post('/notification/accept_team_invitation' , {
                notification_id: data.notificationId,
                team_id: data.teamId,
                user_id: context.state.userId,
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    rejectTeamInvitation(context, data)
    {
        return new Promise((resolve, reject) => {
            axios.post('/notification/reject_team_invitation' , {
                notification_id: data.notificationId,
                team_id: data.teamId,
                user_id: context.state.userId,
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    acceptTeamRegisterRequest(context, data)
    {
        return new Promise((resolve, reject) => {
            axios.post('/notification/accept_team_register_request' , {
                notification_id: data.notificationId,
                team_id: data.teamId,
                sender_user_id: data.senderUserId,
                user_id: context.state.userId,
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    rejectTeamRegisterRequest(context, data)
    {
        return new Promise((resolve, reject) => {
            axios.post('/notification/reject_team_register_request' , {
                notification_id: data.notificationId,
                team_id: data.teamId,
                sender_user_id: data.senderUserId,
                user_id: context.state.userId,
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    acceptRaceInvitation(context, data)
    {
        return new Promise((resolve, reject) => {
            axios.post('/notification/accept_race_invitation' , {
                notification_id: data.notificationId,
                race_id: data.raceId,
                user_id: context.state.userId,
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    rejectRaceInvitation(context, data)
    {
        return new Promise((resolve, reject) => {
            axios.post('/notification/reject_race_invitation' , {
                notification_id: data.notificationId,
                race_id: data.raceId,
                user_id: context.state.userId,
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
}

export default {
    state,
    getters,
    mutations,
    actions,
};