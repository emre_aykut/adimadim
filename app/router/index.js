import MapView from '../components/Map'
import MapTemplate from '../components/MapTemplate'
import Home from '../components/Home'
import Login from '../components/Login'
import Register from '../components/Register'
import Campaign from '../components/Campaign'
import ProfileView from '../components/profile/View'
import ProfileEdit from '../components/profile/Edit'
import RaceList from '../components/race/List'
import RaceDetail from '../components/race/Detail'
import RaceSuccess from '../components/race/Success'
import RaceRegisteredUsers from '../components/race/RegisteredUsers'
import RaceRegisteredTeams from '../components/race/RegisteredTeams'
import SentRaceInvitationsToUsers from '../components/race/SentRaceInvitationsToUsers'
import InviteToRace from '../components/race/inviteToRace'
import TeamChoose from '../components/team/Choose'
import InvitationSentToTeam from '../components/team/InvitationSentToTeam'
import TeamCreate from '../components/team/Create'
import InviteToTeam from '../components/team/InviteToTeam'
import SentInvitationsToUsers from '../components/team/SentInvitationsToUsers'
import TeamInfo from '../components/team/Info'
import ResultList from '../components/result/List'
import ResultStatistic from '../components/result/Statistic'
import Notification from '../components/Notification'
import NotificationDetail from '../components/NotificationDetail'
import NotificationInviteUserToTeam from '../components/notification/InviteUserToTeam'
import NotificationInviteUserToTeamAcceptAndReject from '../components/notification/InviteUserToTeamAcceptAndReject'
import NotificationTeamRegisterRequest from '../components/notification/TeamRegisterRequest'
import NotificationTeamRegisterRequestAcceptAndReject from '../components/notification/TeamRegisterRequestAcceptAndReject'
import NotificationInviteUserToRace from '../components/notification/InviteUserToRace'
import AdminMapTemplate from '../components/admin/MapTemplate'
import AdminRaceList from '../components/admin/RaceList'
import AdminRaceAdd from '../components/admin/RaceAdd'
import AdminRaceEdit from '../components/admin/RaceEdit'
import AdminRacePlanned from '../components/admin/RacePlanned'
import AdminRaceDetail from '../components/admin/RaceDetail'
import AdminTrailList from '../components/admin/TrailList'
import AdminTrailAdd from '../components/admin/TrailAdd'
import AdminTrailEdit from '../components/admin/TrailEdit'
import AdminTrailDetail from '../components/admin/TrailDetail'
import AdminTimeEdit from '../components/admin/TimeEdit'

const router = {
    home: Home,
    login: Login,
    register: Register,
    campaign: Campaign,
    map: MapView,
    mapTemplate: MapTemplate,

    // profile router start
    profileView: ProfileView,
    profileEdit: ProfileEdit,
    // profile router end

    // race router start
    raceList: RaceList,
    raceDetail: RaceDetail,
    raceSuccess: RaceSuccess,
    raceRegisteredUsers: RaceRegisteredUsers,
    raceRegisteredTeams: RaceRegisteredTeams,
    inviteToRace: InviteToRace,
    sentRaceInvitationsToUsers: SentRaceInvitationsToUsers,
    // race router end

    // team router start
    teamChoose: TeamChoose,
    invitationSentToTeam: InvitationSentToTeam,
    teamCreate: TeamCreate,
    inviteToTeam: InviteToTeam,
    sentInvitationsToUsers: SentInvitationsToUsers,
    teamInfo: TeamInfo,
    // team router end

    // result router start
    resultList: ResultList,
    resultStatistic: ResultStatistic,
    // result router end

    // notification router start
    notification: Notification,
    notificationdetail: NotificationDetail,
    notificationInviteUserToTeam: NotificationInviteUserToTeam,
    notificationInviteUserToTeamAcceptAndReject: NotificationInviteUserToTeamAcceptAndReject,
    notificationTeamRegisterRequest: NotificationTeamRegisterRequest,
    notificationTeamRegisterRequestAcceptAndReject: NotificationTeamRegisterRequestAcceptAndReject,
    notificationInviteUserToRace: NotificationInviteUserToRace,
    // notification router end

    // admin router start
    adminMapTemplate: AdminMapTemplate,
    adminRaceList: AdminRaceList,
    adminRaceAdd: AdminRaceAdd,
    adminRaceEdit: AdminRaceEdit,
    adminRacePlanned: AdminRacePlanned,
    adminRaceDetail: AdminRaceDetail,
    adminTrailList: AdminTrailList,
    adminTrailAdd: AdminTrailAdd,
    adminTrailEdit: AdminTrailEdit,
    adminTrailDetail: AdminTrailDetail,
    adminTimeEdit: AdminTimeEdit,
    // admin router end
}

export default router