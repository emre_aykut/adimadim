import {LocalNotifications} from "nativescript-local-notifications";
import localStorage from "nativescript-localstorage";
import { Accuracy } from "tns-core-modules/ui/enums";
import * as http from "tns-core-modules/http";
import moment from 'moment';
import { topmost } from 'tns-core-modules/ui/frame'
import Notification from './components/Notification'
import RaceDetail from './components/race/Detail'

var geolocation = require("nativescript-geolocation");
var watchID;
var baseURL = 'http://adimadim.strajedi.com.tr/api';
// var baseURL = 'http://192.168.0.45:1007/api';

function clearWatch() {
    if (watchID) {
        geolocation.clearWatch(watchID);
        watchID = null;
    }
}

function getLocation() {
    if (geolocation.isEnabled()){

        let raceDetail = JSON.parse(localStorage.getItem('race_detail'));
        let APIKEY = 'AIzaSyASgK-EyVswAr3dln86X7_LtpgWYN9Pf1c';
        let endDistanceInMeters = 0;

        if (raceDetail)
        {       
            var nowDate = moment(String(new Date())).format('YYYY-MM-DD');
            var nowTimeNumber = Math.abs(new Date())
            var raceStartTime = moment(String(raceDetail.startDate+ ' '+ raceDetail.startHour)).format('YYYY/MM/DD HH:mm:ss')
            var raceEndTime = moment(String(raceDetail.startDate+ ' '+ raceDetail.endHour)).format('YYYY/MM/DD HH:mm:ss')
            var raceStartHourNumber = Math.abs(new Date(raceStartTime))
            var raceEndHourNumber = Math.abs(new Date(raceEndTime))
            
            if(!watchID)
            {
                const timerModule = require("tns-core-modules/timer");

                var timerID = timerModule.setInterval(() => {
                    
                    http.getJSON(baseURL + '/race/' + raceDetail.raceId + '?user_id=' + localStorage.getItem('user_id')).then(
                        result => {
                            raceDetail.startDate = result.start_date;
                            raceDetail.startHour = result.start_hour;
                            raceDetail.minTime = result.min_time;
                            raceDetail.maxTime = result.max_time;
                        },
                        error => {
                            console.log(error);
                        }
                    );
                    
                    nowDate = moment(String(new Date())).format('YYYY-MM-DD');
                    nowTimeNumber = Math.abs(new Date())
                    raceStartTime = moment(String(raceDetail.startDate+ ' '+ raceDetail.startHour)).format('YYYY/MM/DD HH:mm:ss')
                    raceEndTime = moment(String(raceDetail.startDate+ ' '+ raceDetail.endHour)).format('YYYY/MM/DD HH:mm:ss')
                    raceStartHourNumber = Math.abs(new Date(raceStartTime))
                    raceEndHourNumber = Math.abs(new Date(raceEndTime))

                    if(nowDate == raceDetail.startDate && nowTimeNumber >= raceStartHourNumber)
                    {
                        let timeCounterControl = parseInt(localStorage.getItem('time_counter'))

                        if (timeCounterControl == 0)
                        {
                            geolocation.enableLocationRequest(true, true).then(() => {
                                geolocation.getCurrentLocation({
                                    desiredAccuracy: Accuracy.high,
                                    maximumAge: 1000,
                                    timeout: 20000
                                })
                                .then(res => {
                                    let currentLatitude = res.latitude;
                                    let currentLongitude = res.longitude;
            
                                    let currentAPIURL = "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&";
                                    currentAPIURL += `origins=${currentLatitude},${currentLongitude}&destinations=${raceDetail.startDestination.latitude},${raceDetail.startDestination.longitude}&mode=walking&language=tr-TR&key=${APIKEY}`;
            
                                    http.getJSON(currentAPIURL).then(
                                        result => {
                                            let startDistanceInMeters = result.rows[0].elements[0].distance.value
                                        
                                            if (startDistanceInMeters < 50) 
                                            {
                                                localStorage.setItem('time_counter', parseInt(localStorage.getItem('time_counter')) + 1)
                                                let timeCounter = parseInt(localStorage.getItem('time_counter'))
                                                
                                                let raceMinute = parseInt(timeCounter / 60) + '';
                                                if (raceMinute.length < 2)
                                                {
                                                    raceMinute = '0' + raceMinute;
                                                }
                                                
                                                let raceSecond = parseInt(timeCounter % 60) + '';
                                                if (raceSecond.length < 2)
                                                {
                                                    raceSecond = '0' + raceSecond;
                                                }
    
                                                let runningTime = raceMinute + ':' + raceSecond;
    
                                                localStorage.setItem('running_time', runningTime)
                                            }
                                            else
                                            {
                                                localStorage.setItem('running_time', 'Koşu başlama noktasından koşuya başlamanız gerekiyor')
                                            }
                                        },
                                        error => {
                                            console.log(error);
                                        }
                                    );
                                })
                                .catch(e => {
                                    console.log("error", e);
                                });
                            }, (e) => {
                                console.log("Error: " + (e.message || e));
                            }).catch(ex => {
                                console.log("Unable to Enable Location", ex);
                            });
                        }
                        else if(timeCounterControl > 0)
                        {
                            localStorage.setItem('time_counter', parseInt(localStorage.getItem('time_counter')) + 1)
                            let timeCounter = parseInt(localStorage.getItem('time_counter'))
                            
                            let raceMinute = parseInt(timeCounter / 60) + '';
                            if (raceMinute.length < 2)
                            {
                                raceMinute = '0' + raceMinute;
                            }
                            
                            let raceSecond = parseInt(timeCounter % 60) + '';
                            if (raceSecond.length < 2)
                            {
                                raceSecond = '0' + raceSecond;
                            }

                            let runningTime = raceMinute + ':' + raceSecond;

                            localStorage.setItem('running_time', runningTime)
                        }

                        if(nowTimeNumber >= raceEndHourNumber)
                        {
                            timerModule.clearInterval(timerID);                        
                            localStorage.removeItem('race_detail')
                            localStorage.removeItem('time_counter')
                            localStorage.removeItem('end_distance')
                            localStorage.removeItem('running_time')
                            clearWatch();

                            LocalNotifications.schedule([{
                                id: watchID,
                                title: "Koşu bitti!",
                                subtitle: "Koşunun bitiş süresini doldurdunuz. Otomatik olarak maksimum süre sonuç tablonuza eklendi.",
                                forceShowWhenInForeground: true,
                                channel: "vue-channel",
                                actions: [
                                    {id: "yes", type: "button", title: "Evet (Uygulamayı Aç)", launch: true},
                                    {id: "no", type: "button", title: "Hayır", launch: false}
                                ]
                            }])

                            http.request({
                                url: baseURL + '/race_user/end',
                                method: 'POST',
                                headers: { "Content-Type": "application/json" },
                                content: JSON.stringify({
                                    race_id: raceDetail.raceId,
                                    user_id: localStorage.getItem('user_id'),
                                })
                            }).then((response) => {
                                const result = response.content.toJSON();
                            
                                let options = options || {
                                    clearHistory: false,
                                    backstackVisible: true,
                                    transition: {
                                        name: "slide",
                                        duration: 300,
                                        curve: "easeIn"
                                    },
                                    props: {
                                        raceId: raceDetail.raceId
                                    }
                                }
                                
                                topmost().currentPage.__vuePageRef__.$navigateTo(RaceDetail, options)
                            }, (e) => {
                                console.log('err: ' + e)
                            });
                        }
                    }
                    else
                    {
                        localStorage.setItem('time_counter', 0)
                        localStorage.setItem('running_time', '00:00')
                        localStorage.setItem('end_distance', '0m')
                    }
                    
                }, 1000);

                watchID = geolocation.watchLocation(
                    res => {
                        if (res) {
                            if(nowDate == raceDetail.startDate && nowTimeNumber >= raceStartHourNumber)
                            {
                                let liveLatitude = res.latitude;
                                let liveLongitude = res.longitude;
                                
                                let distanceMatrixAPIURL = "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&";
                                distanceMatrixAPIURL += `origins=${liveLatitude},${liveLongitude}&destinations=${raceDetail.endDestination.latitude},${raceDetail.endDestination.longitude}&mode=walking&language=tr-TR&key=${APIKEY}`;

                                http.getJSON(distanceMatrixAPIURL).then(
                                    result => {
                                        endDistanceInMeters = result.rows[0].elements[0].distance.value
                                        localStorage.setItem('end_distance', endDistanceInMeters + 'm')        
                                        let raceMinEndTime = moment(String(raceDetail.startDate+ ' '+ raceDetail.startHour)).add(raceDetail.minTime, 'minutes').format('YYYY/MM/DD HH:mm:ss')
                                        let raceMaxEndTime = moment(String(raceDetail.startDate+ ' '+ raceDetail.endHour)).format('YYYY/MM/DD HH:mm:ss')
                                        let minEndTime = Math.abs(new Date(raceMinEndTime))
                                        let maxEndTime = Math.abs(new Date(raceMaxEndTime))
                                        let nowTime = Math.abs(new Date())
                                        if (nowTime > minEndTime)
                                        {
                                            if (endDistanceInMeters <= 15) 
                                            {   
                                                if (watchID)
                                                {
                                                    timerModule.clearInterval(timerID);
                                                    localStorage.removeItem('race_detail')
                                                    localStorage.removeItem('time_counter')
                                                    localStorage.removeItem('end_distance')
                                                    localStorage.removeItem('running_time')
                                                    clearWatch();

                                                    LocalNotifications.schedule([{
                                                        id: watchID,
                                                        title: "Tebrikler! Koşuyu bitirdin.",
                                                        subtitle: "Sonuçlar sayfasından derecenize bakabilirsiniz.",
                                                        forceShowWhenInForeground: true,
                                                        channel: "vue-channel",
                                                        actions: [
                                                            {id: "yes", type: "button", title: "Evet (Uygulamayı Aç)", launch: true},
                                                            {id: "no", type: "button", title: "Hayır", launch: false}
                                                        ]
                                                    }])
                                                }
                                                
                                                http.request({
                                                    url: baseURL + '/race_user/end',
                                                    method: 'POST',
                                                    headers: { "Content-Type": "application/json" },
                                                    content: JSON.stringify({
                                                        race_id: raceDetail.raceId,
                                                        user_id: localStorage.getItem('user_id'),
                                                    })
                                                }).then((response) => {
                                                    const result = response.content.toJSON();
                                                    console.log('result: ' + result.message)

                                                    let options = options || {
                                                        clearHistory: false,
                                                        backstackVisible: true,
                                                        transition: {
                                                            name: "slide",
                                                            duration: 300,
                                                            curve: "easeIn"
                                                        },
                                                        props: {
                                                            raceId: raceDetail.raceId
                                                        }
                                                    }
                                                    
                                                    topmost().currentPage.__vuePageRef__.$navigateTo(RaceDetail, options)
                                                }, (e) => {
                                                    console.log('err: ' + e)
                                                });
                                            }
                                            else
                                            {
                                                console.log('koşuya devam....')
                                            }
                                        }
                                        else
                                        {
                                            console.log('min koşu bitirme süresi daha dolmadı...')
                                        }
                                    },
                                    error => {
                                        console.log(error);
                                    }
                                );
                            }
                        }
                    }, 
                    err => {
                        console.log("Error: " + err.message);
                    }, {
                        desiredAccuracy: Accuracy.high,
                        iosAllowsBackgroundLocationUpdates:true, 
                        showsBackgroundLocationIndicator:false, 
                        iosPausesLocationUpdatesAutomatically: false,
                        updateDistance: 1, 
                        minimumUpdateTime : 1,
                        updateTime: 1,
                    }
                );
            }
        }
    }

}

function getNotification() {

    let receiver_user_id = localStorage.getItem('user_id');
    
    if(localStorage.getItem('user_id'))
    {
        http.getJSON(baseURL + '/notification/new?receiver_user_id=' + receiver_user_id).then(
            result => {
                var data_array = result.data
                if (result.status == true)
                {   
                    data_array.forEach(function(element)
                    {
                        LocalNotifications.schedule([{
                            id: parseInt(element.id),
                            title: element.title,
                            forceShowWhenInForeground: true,
                            channel: "vue-channel",
                            actions: [
                                {id: "yes", type: "button", title: "Evet (Uygulamayı Aç)", launch: true},
                                {id: "no", type: "button", title: "Hayır", launch: false}
                            ]
                        }])

                        LocalNotifications.addOnMessageReceivedCallback(notification => {
                            // console.log("Notification received: " + JSON.stringify(notification));
                            
                            let options = {
                                clearHistory: false,
                                backstackVisible: true,
                                transition: {
                                    name: "fade",
                                    duration: 1,
                                    curve: "easeIn"
                                },
                            }
                            
                            topmost().currentPage.__vuePageRef__.$navigateTo(Notification, options)
                        });
                    });
                }
            },
            error => {
                console.log(error);
            }
        );
    }
}

export function beginLocationUpdates() {
    // geolocation.isEnabled().then(res => console.log("Location Service enabled: "+ res));
    getLocation();
}

export function beginNotificationControl() {
    getNotification();
}
