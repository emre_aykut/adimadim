import  * as BackgroundServices from './backgroundService';

export class CustomAppDelegate extends UIResponder implements UIApplicationDelegate {
    public static ObjCProtocols = [UIApplicationDelegate];
    public static ObjCExposedMethods = {
        "runOnBackground": { returns: interop.types.void }
    };
    
    private timer;
    private timerCounter;

    public applicationDidBecomeActive(application: UIApplication) {
        console.log('UYGULAMA BAŞLATILDI.')
        this.timerCounter = 1;
        this.timer = NSTimer.scheduledTimerWithTimeIntervalTargetSelectorUserInfoRepeats(1, this, "runOnBackground", null, true);
    }

    public applicationDidEnterBackground(application: UIApplication) {
        console.log("Enter background");
    }

    public applicationDidFinishLaunchingWithOptions(application: UIApplication,  launchOptions: NSDictionary<string, any>): boolean {
        application.setMinimumBackgroundFetchInterval(100)
        return true;
    }

    public applicationPerformFetchWithCompletionHandler(application: UIApplication, completionHandler: any) {
        BackgroundServices.beginNotificationControl();
    }

    public runOnBackground(): void {
        BackgroundServices.beginLocationUpdates();
    }
    
}